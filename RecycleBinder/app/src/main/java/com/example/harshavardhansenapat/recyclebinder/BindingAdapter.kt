package com.example.harshavardhansenapat.recyclebinder

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/*
@BindingAdapter("src")
fun setImg(imageView: ImageView, image: Int) {
    imageView.setImageResource(image)
}
*/

@BindingAdapter("bind:item","bind:childLayout")
fun setEmpList(recyclerView: androidx.recyclerview.widget.RecyclerView,items: ObservableArrayList<User>,childlayout: Int) {
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
//    recyclerView.layoutManager = recyclerView.layoutManager
    val adapter = RecyclerViewAdapter(recyclerView,items,childlayout)
    recyclerView.adapter = adapter
    adapter.notifyDataSetChanged()
}