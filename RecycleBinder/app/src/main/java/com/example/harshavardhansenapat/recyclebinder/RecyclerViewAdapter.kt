package com.example.harshavardhansenapat.recyclebinder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR


/**
 * This is the common adapter class for Recycler view with child layout binding
 **/
class RecyclerViewAdapter(val view: androidx.recyclerview.widget.RecyclerView, items: ObservableArrayList<*>, childLayout: Int) : androidx.recyclerview.widget.RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {
    private var mChildLayoutId: Int = childLayout
    var mListObjects:List<*> = items
    var size = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.RecyclerViewHolder {
        val viewHolder: RecyclerViewAdapter.RecyclerViewHolder
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater!!, mChildLayoutId, parent, false)
        viewHolder = RecyclerViewHolder(binding = binding)
        size = mListObjects.size
        return viewHolder

    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.RecyclerViewHolder, position: Int) {
        val item= getItem(position)
        holder.bindItems(item,position)

    }
    private fun getItem(position: Int): Any? {
        return mListObjects[position]
    }

    override fun getItemCount(): Int {
        return mListObjects.size
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    inner class RecyclerViewHolder(val binding: ViewDataBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bindItems(item:Any?, position: Int) {
            binding.setVariable(BR.user,item)
           /* binding.setVariable(BR.position,position)
            binding.setVariable(BR.dataPoolHandler, DataPoolHandler)*/

        }

    }
}